import UIKit

final class ListViewController: UIViewController {
        
    lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.dataSource = self
        view.delegate = self
        view.register(ImageCollectionViewCell.self, forCellWithReuseIdentifier: "cellid")
        return view
    }()
    
    private lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        //Will be overriden by UICollectionViewDelegateFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom:10, right: 10)
        layout.itemSize = CGSize(width: 75, height: 50)
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        layout.headerReferenceSize = CGSize(width: CGFloat.leastNonzeroMagnitude,
                                            height: CGFloat.leastNonzeroMagnitude)
        return layout
    }()
    
    private var adapterList = [UICollectionViewCell: ReusableCellImageAdapter]()
    
    //MARK: - Lifecycle
    deinit{
        print("dealloc \(self)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        
        buildStyle()
        buildConstraints()
    }
    
    private func buildStyle() {
        title = "List" //TODO : Add localization
        collectionView.backgroundColor = .white
    }
    
    private func buildConstraints() {
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: guide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: guide.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: guide.rightAnchor),
        ])
    }
    
    private func getReusableAdapter(forReusableCell cell: UICollectionViewCell) -> ReusableCellImageAdapter{
        if let adapter = adapterList[cell]{
            //print("Find a reusable adapter")
            return adapter
        }
        else{
            //print("Create a reusable adapter")
            let adapter = ReusableCellImageAdapter()
            adapterList[cell] = adapter
            return adapter
        }
    }
}

extension ListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did select item at \(indexPath)")
        let vc = UIViewController()
        vc.view.backgroundColor = .systemGreen
        if let navigationController = navigationController {
            navigationController.pushViewController(vc, animated: true)
        }
        else {
            present(vc, animated: true)
        }
    }
}

extension ListViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 110
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath) as? ImageCollectionViewCell else { return UICollectionViewCell() }
        let filename = "\(indexPath.row).jpg"
        let baseUrl = "https://bitbucket.org/manu2/data/raw/master/images/"
        let path = baseUrl + filename
        let adapter = getReusableAdapter(forReusableCell: cell)
        cell.imageView.image = nil
        cell.imageView.backgroundColor = .systemBlue
        cell.activityView.startAnimating()
        adapter.configure(from: path) { image in
            cell.activityView.stopAnimating()
            if let image = image {
                cell.imageView.image = image
            }
            else{
                cell.imageView.backgroundColor = .systemRed
            }
        }
        return cell
    }
}

#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct ListViewControllerRepresentable: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        let vc = ListViewController()
        vc.collectionView.backgroundColor = .white
        vc.view.backgroundColor = .brown
        return vc.view
    }
    
    func updateUIView(_ view: UIView, context: Context) {
    }
}

@available(iOS 13.0, *)
struct ListViewControllerRepresentablePreview: PreviewProvider {
    static var previews: some View {
        ListViewControllerRepresentable().previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
    }
}
#endif
