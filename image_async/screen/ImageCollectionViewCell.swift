import UIKit

final class ImageCollectionViewCell : UICollectionViewCell {
    
    private(set) var imageView = UIImageView()
    private(set) var activityView = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildSubviews()
        buildConstraints()
        buildStyle()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("dealloc \(self)")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
        imageView.backgroundColor = .white
    }
    
    private func buildSubviews() {
        contentView.addSubview(imageView)
        contentView.addSubview(activityView)
    }
    
    private func buildConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
        activityView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            
            activityView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            activityView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        ])
    }
    
    private func buildStyle() {
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 8.0
        imageView.layer.masksToBounds = true
        contentView.layer.cornerRadius = 8.0
        contentView.layer.shadowColor = UIColor.gray.cgColor
        contentView.layer.shadowOpacity = 0.8
        contentView.layer.shadowRadius = 2
        contentView.layer.shadowOffset = CGSize(width: 5, height: 5)
    }
    
    func errorStyle() {
        imageView.backgroundColor = .orange
        imageView.image = nil
    }
    
    func standardStyle() {
        imageView.backgroundColor = .white
    }
}

#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct ImageCollectionViewCellRepresentable: UIViewRepresentable {
  func makeUIView(context: Context) -> UIView {
    let cell = ImageCollectionViewCell()
    cell.backgroundColor = .clear
    cell.imageView.backgroundColor = .systemGreen
    cell.imageView.image = UIImage(systemName: "sun.min.fill")
    return cell
  }
  
  func updateUIView(_ view: UIView, context: Context) {
  }
}

@available(iOS 13.0, *)
struct ImageCollectionViewCellRepresentablePreview: PreviewProvider {
  static var previews: some View {
    ImageCollectionViewCellRepresentable().frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
  }
}
#endif
